# HexPropsEzWalls

## Summary

This is fork of <https://github.com/Hexer10/HexProps> with a new command
`!ezwall` which allows the player to add "walls" to the map with minimal effort.

The mod also loads all the props when loading the map.

## Installation

Find the build artifacts and download the archive from
<https://gitlab.com/niklashh/hexpropsezwalls/-/releases>

Extract the archive in the server's `csgo` folder

Using my csgo docker image from [steamutils](https://gitlab.com/niklashh/steamutils):

```shell
docker run --rm --volume csgo-dedicated:/home/steam/csgo-dedicated -it niklashh/csgo:latest install-targz-url https://gitlab.com/niklashh/hexpropsezwalls/-/jobs/<job number>/artifacts/file/build/hexpropsezwalls.tar.gz
```

## Building

The included dockerfile builds the source code and archives it in
`/home/steam/hexpropsezwalls.tar.gz`

```shell
docker build -t hexpropsezwalls .
```

The archive can be copied over with a little docker run command

```shell
docker run --mount type=bind,source="$(pwd)"/build,target=/build --entrypoint cp hexpropsezwalls /home/steam/hexpropsezwalls.tar.gz /build
docker run --mount type=bind,source="$(pwd)"/build,target=/build --entrypoint cp hexpropsezwalls /home/steam/addons/sourcemod/plugins/hexprops.smx /build
```

🤷
