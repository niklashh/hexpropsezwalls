FROM niklashh/sourcepawn:latest

COPY --chown=steam ./addons ./addons

RUN [ "/bin/bash", \
    "-c", \
    "./addons/sourcemod/scripting/compile.sh hexprops.sp \
    && mv addons/sourcemod/scripting/compiled/hexprops.smx addons/sourcemod/plugins \
    && tar cvf hexpropsezwalls.tar.gz \
    addons/sourcemod/scripting/{hexprops.sp,include/hexprops.inc,include/hexstocks.inc} \
    addons/sourcemod/plugins/hexprops.smx \
    addons/sourcemod/configs/props/props_list.txt" ]
